========
Overview
========

BMHData is a new CRD that capture templates and generates secrets from them for each
BareMetalHost or Kubevirt virtual machine that reference them using information stored
on their resource to specialize the template.

* Reference to a BMHData is done through a pre-defined label
* Templates can be used for pre-provisioning network data (the initial use case)
  or even user-data and network-data (used by the now obsolete BYOH approach)
* Template contents is arbitrary text using go template to dynamically generate
  content.

Operation
=========

The BMHData resource and the BareMetalHost (or VirtualMachine) must be in the same
namespace. The host must contain a label ``kanod.io/bmh-data`` containing the name
of the BMHData resource. The secrets generated depend on the existence of
reference to templates in the specification of BMHData:

* ``preprovisioningNetwork`` is the name of the secret containing the template
  of a network configuration under the key ``networkData`` for the Ironic Python
  Agent. If defined a secret
  is generated and its reference is put in ``preprovisioningNetworkDataName``
  of the BareMetalHost (not available for VirtualMachine).
* ``userData`` and ``networkData`` are the names of a secret containing a
  template of either user or network cloud-init configurations for either
  the BareMetalHost or the VirtualMachine.  The respective keys in the secret
  templates are ``userData`` and ``networkData``. The generated secrets names are put
  in the intended fields on the compute resources (The use case for these is
  *deprecated*. It was mainly used by pools of compute resources when using the
  BringYourOwnHost provisioner). Note that Kubevirt does not really support 
  user supplied metaData. Early expansion of userData was necessary for this 
  use case.
* If generateMetadata is set to true, metadata are also generated from the keys
  contained in the BMHData.

During the generation of ``preprovisioningNetwork``, the BareMetalHost **must**
be paused. Otherwise there is a race with the inspection mechanism of Ironic.
If the pause is trigerred with  ``baremetalhost.metal3.io/paused`` set to
``network.kanod.io/bmh-data``, the pause annotation will be automatically
removed at the end of the template generation. Otherwise, one can use
the annotation ``kanod.io/bmh-data-version`` on the BareMetalHost as a marker
for removing the pause annotation.

Templates and Keys
==================

Templates are go templates refering to keys defining a context for the expansion.
To avoid conflict with other templating engines, one must use ``{{: :}}`` instead
of the standard ``{{ }}`` delimiters.
The keys are specified in the BMHData specification:

* most fields are the counter parts of Metal3DataTemplates and refer either to
  labels, annotations of the baremetalHost, ipam generated values.
* a predefined key ``boot_mac_address`` contains the boot mac address of the
  BareMetalHost.

The keys can also be used to generate metadata resources for bareMetalHosts.


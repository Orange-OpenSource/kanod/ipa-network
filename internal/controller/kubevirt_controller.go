/*
Copyright 2023 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/go-logr/logr"
	m3ipam "github.com/metal3-io/ip-address-manager/api/v1alpha1"
	bmhdatav1beta1 "gitlab.com/Orange-OpenSource/kanod/bmh-data/api/v1beta1"
	v1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	kubevirtv1 "kubevirt.io/api/core/v1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logger "sigs.k8s.io/controller-runtime/pkg/log"
)

/* Controller for adding a cloudinit disk to a labelled virtual machine
   with */

// KubevirtReconciler reconciles a virtualMachine object
type KubevirtReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

// +kubebuilder:rbac:groups=kubevirt.io,resources=virtualmachines,verbs=get;list;watch;create;update;patch;delete

func (r *KubevirtReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := logger.FromContext(ctx).WithValues("namespace", req.Namespace, "virtualMachine", req.Name)
	log.V(Debug).Info("Reconciling")
	virtualMachine := &kubevirtv1.VirtualMachine{}
	err := r.Get(ctx, req.NamespacedName, virtualMachine)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("VirtualMachine resource not found. Ignoring since object must be deleted")
			return ctrl.Result{}, nil
		}
		log.Error(err, "Failed to get Virtual Machine")
		return ctrl.Result{}, err
	}

	if virtualMachine.Labels == nil {
		return ctrl.Result{}, nil
	}
	bmhDataName, ok := virtualMachine.Labels[BmhDataLabel]
	if !ok {
		// No label so nothing to do
		return ctrl.Result{}, nil
	}
	log = log.WithValues("bmhData", bmhDataName)
	log.V(Debug).Info("Accessing BmhData custom resource")
	bmhData := &bmhdatav1beta1.BmhData{}
	key := types.NamespacedName{Namespace: req.Namespace, Name: bmhDataName}
	err = r.Get(ctx, key, bmhData)
	if err != nil {
		log.Error(err, "Failed to get BmhData", "bmhDataName", bmhDataName)
		return ctrl.Result{}, err
	}
	version, ok := virtualMachine.Annotations[BmhDataVersionAnnotation]
	var needLinks bool
	if ok && version == bmhData.ResourceVersion {
		log.V(Debug).Info("Already to last version.")
	} else {
		var requeue bool
		needLinks, requeue, err = r.renderVirtualMachineData(virtualMachine, bmhData, log)
		if err != nil {
			return ctrl.Result{}, err
		}
		if requeue {
			return ctrl.Result{}, nil
		}
	}

	if !ok || needLinks {
		log.V(Debug).Info("Linking preprovisioning data in BareMetalHost")
		if err = r.setLinks(virtualMachine, bmhData, log); err != nil {
			return ctrl.Result{}, err
		}
	}

	if virtualMachine.Annotations != nil && virtualMachine.Annotations["bmhdata.kanod.io/start"] == "true" {
		if err = r.startVm(virtualMachine, log); err != nil {
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *KubevirtReconciler) SetupWithManager(mgr ctrl.Manager) error {
	controller := ctrl.NewControllerManagedBy(mgr).
		For(&kubevirtv1.VirtualMachine{})
	if HasCrd(mgr, "ipclaims.ipam.metal3.io") {
		logger.Log.Info("Kubevirt controller: Support for Metal3 IPAM: ok")
		controller = controller.Watches(
			&m3ipam.IPClaim{},
			handler.EnqueueRequestsFromMapFunc(Metal3IPClaimToVirtualMachine),
		)
	} else {
		logger.Log.Info("Kubevirt controller: support for Metal3 IPAM: fail - Not watching IPClaims")
	}
	return controller.Complete(r)
}

func (r *KubevirtReconciler) renderVirtualMachineData(
	virtualMachine *kubevirtv1.VirtualMachine, bmhData *bmhdatav1beta1.BmhData, log logr.Logger,
) (bool, bool, error) {
	needLinks := false
	ctx := context.Background()
	templateContext, _, requeue, err := generateConf(
		r.Client, log, bmhData, virtualMachine, virtualMachine.GroupVersionKind(),
	)
	if requeue || err != nil {
		return false, requeue, err
	}
	templateName := bmhData.Spec.NetworkData
	if templateName != "" {
		needLinks = true
		err = generateSecret(
			r.Client, ctx, log, templateName, virtualMachine,
			virtualMachine.GroupVersionKind(), bmhData,
			templateContext, "networkData", "network-data")
		if err != nil {
			return false, false, err
		}
	}
	templateName = bmhData.Spec.UserData
	if templateName != "" {
		needLinks = true
		err = generateSecret(
			r.Client, ctx, log, templateName, virtualMachine,
			virtualMachine.GroupVersionKind(), bmhData,
			templateContext, "userData", "user-data")
		if err != nil {
			return false, false, err
		}
	}

	return needLinks, false, nil
}

func (r *KubevirtReconciler) startVm(virtualMachine *kubevirtv1.VirtualMachine, log logr.Logger) error {
	log.Info("Starting Virtual Machine")
	content := []map[string]interface{}{{"op": "replace", "path": "/spec/running", "value": true}}
	data, err := json.Marshal(content)
	if err != nil {
		log.Error(err, "Cannot marshal json data")
		return err
	}
	patch := client.RawPatch(types.JSONPatchType, data)
	err = r.Client.Patch(context.Background(), virtualMachine, patch)
	if err != nil {
		log.Error(err, "Failed to remove paused annotation")
		return err
	}

	return nil
}

func (r *KubevirtReconciler) setLinks(
	virtualMachine *kubevirtv1.VirtualMachine,
	bmhData *bmhdatav1beta1.BmhData,
	log logr.Logger,
) error {
	version := bmhData.ResourceVersion
	generateUserData := (bmhData.Spec.UserData != "")
	generateNetworkData := (bmhData.Spec.NetworkData != "")
	userDataConfName := fmt.Sprintf("%s-user-data", virtualMachine.Name)
	networkDataConfName := fmt.Sprintf("%s-network-data", virtualMachine.Name)
	updated := virtualMachine.DeepCopy()
	source := &kubevirtv1.CloudInitNoCloudSource{}
	if generateUserData {
		source.UserDataSecretRef = &v1.LocalObjectReference{
			Name: userDataConfName,
		}
	}
	if generateNetworkData {
		source.NetworkDataSecretRef = &v1.LocalObjectReference{
			Name: networkDataConfName,
		}
	}
	updated.Spec.Template.Spec.Volumes = append(
		updated.Spec.Template.Spec.Volumes,
		kubevirtv1.Volume{
			Name: "cloud-init",
			VolumeSource: kubevirtv1.VolumeSource{
				CloudInitNoCloud: source,
			},
		})
	updated.Spec.Template.Spec.Domain.Devices.Disks = append(
		updated.Spec.Template.Spec.Domain.Devices.Disks,
		kubevirtv1.Disk{
			Name: "cloud-init",
			DiskDevice: kubevirtv1.DiskDevice{
				Disk: &kubevirtv1.DiskTarget{
					Bus: kubevirtv1.DiskBusVirtio,
				},
			},
		},
	)
	updated.Annotations[BmhDataVersionAnnotation] = version
	err := r.Client.Update(context.Background(), updated)
	if err != nil {
		log.Error(err, "Cannot update VirtualMachine with links")
		return err
	}
	return nil
}

// Metal3IPClaimToVirtualMachine will return a reconcile request for a BareMetalHost if the event is for a
// Metal3IPClaim and that Metal3IPClaim references a BareMetalHost.
func Metal3IPClaimToVirtualMachine(ctx context.Context, obj client.Object) []ctrl.Request {
	requests := []ctrl.Request{}
	if bmh, ok := obj.(*m3ipam.IPClaim); ok {
		for _, ownerRef := range bmh.OwnerReferences {
			if ownerRef.Kind != "BareMetalHost" {
				continue
			}
			aGV, err := schema.ParseGroupVersion(ownerRef.APIVersion)
			if err != nil {
				continue
			}
			if aGV.Group != kubevirtv1.GroupVersion.Group {
				continue
			}
			requests = append(requests, ctrl.Request{
				NamespacedName: types.NamespacedName{
					Name:      ownerRef.Name,
					Namespace: bmh.Namespace,
				},
			})
		}
	}
	return requests
}

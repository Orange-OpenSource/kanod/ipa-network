/*
Copyright 2025 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"text/template"

	"sigs.k8s.io/yaml"

	"github.com/go-logr/logr"
	bmhv1alpha1 "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	m3ipam "github.com/metal3-io/ip-address-manager/api/v1alpha1"
	bmhdatav1beta1 "gitlab.com/Orange-OpenSource/kanod/bmh-data/api/v1beta1"
	corev1 "k8s.io/api/core/v1"
	apiextensions "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logger "sigs.k8s.io/controller-runtime/pkg/log"
)

const (
	BmhDataLabel             = "network.kanod.io/bmh-data"
	BmhAnnotationPrefix      = "kanod.io"
	BmhDataVersionAnnotation = "network.kanod.io/bmh-data-version"
	PausedAnnotationValue    = "network.kanod.io/bmh-data"
	BmhDataController        = "bmh-data-controller"
	Debug                    = 1
	CaCertPath               = "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt"
)

// BareMetalHostReconciler reconciles a BareMetalHost object
type BareMetalHostReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

var EnableBMHNameBasedPreallocation bool

//nolint:lll
// +kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts/finalizers,verbs=update
// +kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=apiextensions.k8s.io,resources=customresourcedefinitions,verbs=get;list;watch;create;update;patch
// +kubebuilder:rbac:groups=ipam.metal3.io,resources=ippools,verbs=get;list;watch
// +kubebuilder:rbac:groups=ipam.metal3.io,resources=ipclaims,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=ipam.metal3.io,resources=ipaddresses,verbs=get;list;watch
// +kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=bootstrapkubeconfigs,verbs=get;list;watch;create;update;patch;delete

func (r *BareMetalHostReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := logger.FromContext(ctx).WithValues("namespace", req.Namespace, "BareMetalHost", req.Name)

	bareMetalHost := &bmhv1alpha1.BareMetalHost{}
	err := r.Get(ctx, req.NamespacedName, bareMetalHost)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("BareMetalHost resource not found. Ignoring since object must be deleted")
			return ctrl.Result{}, nil
		}
		log.Error(err, "Failed to get BareMetalHost")
		return ctrl.Result{}, err
	}

	if bareMetalHost.Labels == nil {
		return ctrl.Result{}, nil
	}
	// pivot is under way. Stop immediately
	if bareMetalHost.Labels[bmhv1alpha1.PausedAnnotation] == "metal3.io/capm3" {
		return ctrl.Result{}, nil
	}
	bmhDataName, ok := bareMetalHost.Labels[BmhDataLabel]
	if !ok {
		// No label so nothing to do
		return ctrl.Result{}, nil
	}
	log = log.WithValues("bmhData", bmhDataName)
	log.V(Debug).Info("Accessing BmhData custom resource")
	bmhData := &bmhdatav1beta1.BmhData{}
	key := types.NamespacedName{Namespace: req.Namespace, Name: bmhDataName}
	err = r.Get(ctx, key, bmhData)
	if err != nil {
		log.Error(err, "Failed to get BmhData", "bmhDataName", bmhDataName)
		return ctrl.Result{}, err
	}
	version, ok := bareMetalHost.Annotations[BmhDataVersionAnnotation]
	var needLinks bool
	var bmhAnnotations map[string]string
	if ok && version == bmhData.ResourceVersion {
		log.V(Debug).Info("Already to last version.")
	} else {
		var requeue bool
		needLinks, bmhAnnotations, requeue, err = r.renderBmhData(bareMetalHost, bmhData, log)
		if err != nil {
			return ctrl.Result{}, err
		}
		if requeue {
			log.V(Debug).Info("Requueing")
			return ctrl.Result{}, nil
		}
	}

	if !ok || needLinks {
		log.V(Debug).Info("Linking preprovisioning data in BareMetalHost")
		if err = r.setLinks(bareMetalHost, bmhData, bmhAnnotations, log); err != nil {
			return ctrl.Result{}, err
		}
	}

	if bareMetalHost.Annotations != nil &&
		bareMetalHost.Annotations[bmhv1alpha1.PausedAnnotation] == PausedAnnotationValue {
		if err = r.removePauseAnnotation(bareMetalHost, log); err != nil {
			return ctrl.Result{}, err
		}
	}
	return ctrl.Result{}, nil
}

func HasCrd(mgr ctrl.Manager, name string) bool {
	crd := &apiextensions.CustomResourceDefinition{}
	key := types.NamespacedName{Name: name}
	ctx := context.Background()
	err := mgr.GetAPIReader().Get(ctx, key, crd)
	if err != nil {
		logger.Log.Error(err, "Cannot find CRD", "crd", name)
	}
	return err == nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *BareMetalHostReconciler) SetupWithManager(mgr ctrl.Manager) error {
	controller := ctrl.NewControllerManagedBy(mgr).
		For(&bmhv1alpha1.BareMetalHost{})
	if HasCrd(mgr, "ipclaims.ipam.metal3.io") {
		logger.Log.Info("Support for Metal3 IPAM: ok")
		controller = controller.Watches(
			&m3ipam.IPClaim{},
			handler.EnqueueRequestsFromMapFunc(Metal3IPClaimToBareMetalHost),
		)
	} else {
		logger.Log.Info("Support for Metal3 IPAM: fail - Not watching IPClaims")
	}
	return controller.Complete(r)
}

func generateConf(
	r client.Client,
	log logr.Logger,
	bmhData *bmhdatav1beta1.BmhData,
	object metav1.Object,
	gvk schema.GroupVersionKind,
) (map[string]interface{}, map[string]string, bool, error) {
	log.V(Debug).Info("Building BmhData template context")
	requeueIpam, templateContext, bmhAnnotation, err := buildIpamContext(
		r, log, bmhData, object, gvk)
	if err != nil {
		return nil, nil, false, err
	}

	// We expect the watch on IPA to retriger.
	if requeueIpam {
		return nil, nil, true, nil
	}
	if err := addMetadata(bmhData, object, templateContext); err != nil {
		return nil, nil, false, err
	}
	for _, elt := range bmhData.Spec.Strings {
		templateContext[elt.Key] = elt.Value
	}
	return templateContext, bmhAnnotation, false, nil
}

func (r *BareMetalHostReconciler) renderBmhData(
	bareMetalHost *bmhv1alpha1.BareMetalHost, bmhData *bmhdatav1beta1.BmhData, log logr.Logger,
) (bool, map[string]string, bool, error) {
	needLinks := false
	ctx := context.Background()
	templateContext, bmhAnnotations, requeue, err := generateConf(
		r.Client, log, bmhData, bareMetalHost, bareMetalHost.GroupVersionKind(),
	)
	if requeue || err != nil {
		return false, nil, requeue, err
	}

	templateContext["boot_mac_address"] = bareMetalHost.Spec.BootMACAddress

	templateName := bmhData.Spec.PreprovioningNetwork
	if templateName != "" {
		needLinks = true
		err = generateSecret(
			r.Client, ctx, log, templateName, bareMetalHost,
			bareMetalHost.GroupVersionKind(), bmhData,
			templateContext, "networkData", "ipa-network")
		if err != nil {
			return false, nil, false, err
		}
	}
	templateName = bmhData.Spec.NetworkData
	if templateName != "" {
		needLinks = true
		err = generateSecret(
			r.Client, ctx, log, templateName, bareMetalHost,
			bareMetalHost.GroupVersionKind(), bmhData,
			templateContext, "networkData", "network-data")
		if err != nil {
			return false, nil, false, err
		}
	}
	// For completeness but should not be used on BMH
	templateName = bmhData.Spec.UserData
	if templateName != "" {
		needLinks = true
		err = generateSecret(
			r.Client, ctx, log, templateName, bareMetalHost,
			bareMetalHost.GroupVersionKind(), bmhData,
			templateContext, "userData", "user-data")
		if err != nil {
			return false, nil, false, err
		}
	}

	if bmhData.Spec.GenerateMetadata {
		err = r.generateMetadata(ctx, log, bareMetalHost, templateContext)
		if err != nil {
			return false, nil, false, err
		}
	}
	return needLinks, bmhAnnotations, false, nil
}

func generateSecret(
	cl client.Client,
	ctx context.Context,
	log logr.Logger,
	templateName string,
	bareMetalHost metav1.Object,
	gvk schema.GroupVersionKind,
	bmhData *bmhdatav1beta1.BmhData,
	bmhDataContext map[string]interface{},
	secretKey string,
	suffix string,
) error {
	log.V(Debug).Info("Getting IPA network template secret")
	if templateName == "" {
		log.Info("No template provided")
		return nil
	}
	templateSecret := &corev1.Secret{}
	namespace := bareMetalHost.GetNamespace()
	key := types.NamespacedName{Namespace: namespace, Name: templateName}
	err := cl.Get(ctx, key, templateSecret)
	if err != nil {
		log.Error(
			err, "Failed to get IPA Network template secret",
			"templateName", templateName)
		return err
	}
	templateString, ok := templateSecret.Data["template"]
	if !ok {
		err = fmt.Errorf("no template in secret")
		log.Error(
			err, "No template found in secret",
			"bmhDataName", bmhData.Name, "templateName", templateName)
		return err
	}
	log.V(Debug).Info("Parsing IPA network template")
	tmpl, err := template.
		New(templateName).
		Delims("{{:", ":}}").
		Option("missingkey=error").
		Parse(string(templateString))

	if err != nil {
		log.Error(
			err, "Cannot parse template",
			"bmhDataName", bmhData.Name, "templateName", templateName)
		return err
	}
	log.V(Debug).Info("Rendering IPA template")
	var buffer bytes.Buffer
	err = tmpl.Execute(&buffer, bmhDataContext)
	if err != nil {
		log.Error(
			err, "Cannot render template",
			"bmhDataName", bmhData.Name,
			"template", string(templateString),
			"context", bmhDataContext)
		return err
	}
	log.V(Debug).Info("Generating preprovisioning data")
	secret := &corev1.Secret{}
	secretName := fmt.Sprintf("%s-%s", bareMetalHost.GetName(), suffix)
	secret.ObjectMeta = metav1.ObjectMeta{
		Name:      secretName,
		Namespace: namespace,
	}
	_, err = controllerutil.CreateOrUpdate(
		ctx, cl, secret,
		func() error {
			if secret.ObjectMeta.CreationTimestamp.IsZero() {
				secret.ObjectMeta.OwnerReferences =
					[]metav1.OwnerReference{
						*metav1.NewControllerRef(bareMetalHost, gvk),
					}
				secret.Data = make(map[string][]byte)
			}
			secret.Data[secretKey] = buffer.Bytes()
			return nil
		})
	if err != nil {
		log.Error(err, "Cannot create IPA network configuration secret")
		return err
	}
	return nil
}

func (r *BareMetalHostReconciler) generateMetadata(
	ctx context.Context,
	log logr.Logger,
	bareMetalHost *bmhv1alpha1.BareMetalHost,
	bmhDataContext map[string]interface{},
) error {
	log.V(Debug).Info("Generating preprovisioning data")
	metadata := &corev1.Secret{}
	metadataName := fmt.Sprintf("%s-metadata", bareMetalHost.Name)
	metadata.ObjectMeta = metav1.ObjectMeta{
		Name:      metadataName,
		Namespace: bareMetalHost.Namespace,
	}
	data := make(map[string]string)
	// map values are either strings or array of strings for DNS. Collapse
	// everything to string for metadata.
	for key, value := range bmhDataContext {
		str, ok := value.(string)
		if ok {
			data[key] = str
		} else {
			strArray, ok := value.([]string)
			if ok {
				str, err := json.Marshal(strArray)
				if err != nil {
					log.Error(err, "Bad marshaling of array", "key", key)
					return err
				}
				data[key] = string(str)
			} else {
				err := fmt.Errorf("unknown type for value associated to %s", key)
				return err
			}
		}
	}
	dataBytes, err := yaml.Marshal(data)
	if err != nil {
		log.Error(err, "Cannot marshal metadata")
	}
	_, err = controllerutil.CreateOrUpdate(
		ctx, r.Client, metadata,
		func() error {
			if metadata.ObjectMeta.CreationTimestamp.IsZero() {
				metadata.ObjectMeta.OwnerReferences =
					[]metav1.OwnerReference{ownedBy(bareMetalHost)}
			}
			if metadata.Data == nil {
				metadata.Data = make(map[string][]byte)
			}
			metadata.Data["metaData"] = dataBytes
			return nil
		})
	if err != nil {
		log.Error(err, "Cannot create bareMetalHost metadata secret")
	}
	return err
}

func (r *BareMetalHostReconciler) removePauseAnnotation(
	bareMetalHost *bmhv1alpha1.BareMetalHost,
	log logr.Logger,
) error {
	log.Info("Removing paused annotation from BareMetalHost")
	path := fmt.Sprintf("/metadata/annotations/%s", strings.ReplaceAll(bmhv1alpha1.PausedAnnotation, "/", "~1"))
	content := []map[string]string{{"op": "remove", "path": path}}
	data, err := json.Marshal(content)
	if err != nil {
		log.Error(err, "Cannot marshal json data")
		return err
	}
	patch := client.RawPatch(types.JSONPatchType, data)
	err = r.Client.Patch(context.Background(), bareMetalHost, patch)
	if err != nil {
		log.Error(err, "Failed to remove paused annotation")
		return err
	}

	return nil
}

func (r *BareMetalHostReconciler) setLinks(
	bareMetalHost *bmhv1alpha1.BareMetalHost,
	bmhData *bmhdatav1beta1.BmhData,
	bmhAnnotations map[string]string,
	log logr.Logger,
) error {
	log.Info("Update BareMetalHost with pointer to metadata/networkdata/userdata")

	version := bmhData.ResourceVersion
	generateMeta := bmhData.Spec.GenerateMetadata
	generatePreprovisioning := (bmhData.Spec.PreprovioningNetwork != "")
	generateUserData := (bmhData.Spec.UserData != "")
	generateNetworkData := (bmhData.Spec.NetworkData != "")
	preProDataConfName := fmt.Sprintf("%s-ipa-network", bareMetalHost.Name)
	metaDataConfName := fmt.Sprintf("%s-metadata", bareMetalHost.Name)
	userDataConfName := fmt.Sprintf("%s-user-data", bareMetalHost.Name)
	networkDataConfName := fmt.Sprintf("%s-network-data", bareMetalHost.Name)
	var u unstructured.Unstructured
	u.SetKind(bareMetalHost.Kind)
	u.SetAPIVersion(bareMetalHost.APIVersion)
	u.SetName(bareMetalHost.Name)
	u.SetNamespace(bareMetalHost.Namespace)

	annotations := bmhAnnotations
	annotations[BmhDataVersionAnnotation] = version
	log.Info("Update BareMetalHost annotations", "annotations addons", annotations)
	u.SetAnnotations(annotations)

	spec := map[string]interface{}{}
	u.Object["spec"] = spec
	if generatePreprovisioning {
		spec["preprovisioningNetworkDataName"] = preProDataConfName
	}
	if generateMeta {
		spec["metaData"] = map[string]interface{}{
			"name":      metaDataConfName,
			"namespace": bareMetalHost.Namespace,
		}
	}
	if generateUserData {
		spec["userData"] = map[string]interface{}{
			"name":      userDataConfName,
			"namespace": bareMetalHost.Namespace,
		}
	}
	if generateNetworkData {
		spec["networkData"] = map[string]interface{}{
			"name":      networkDataConfName,
			"namespace": bareMetalHost.Namespace,
		}
	}

	force := true
	err := r.Client.Patch(context.Background(), &u, client.Apply, &client.PatchOptions{
		Force:        &force,
		FieldManager: BmhDataController,
	})
	if err != nil {
		log.Error(err, "Cannot update BareMetalHost with pointer to network data")
		return err
	}
	return nil
}

func addMetadata(
	bmhData *bmhdatav1beta1.BmhData,
	object metav1.Object,
	bmhDataContext map[string]interface{},
) error {
	labels := object.GetLabels()
	if labels != nil {
		for _, elt := range bmhData.Spec.FromLabels {
			v, ok := labels[elt.Label]
			if ok {
				bmhDataContext[elt.Key] = v
			} else {
				return fmt.Errorf("missing label %s", elt.Label)
			}
		}
	} else if len(bmhData.Spec.FromLabels) > 0 {
		return fmt.Errorf("no labels but some extracted")
	}
	annotations := object.GetAnnotations()
	if annotations != nil {
		for _, elt := range bmhData.Spec.FromAnnotations {
			v, ok := annotations[elt.Annotation]
			if ok {
				bmhDataContext[elt.Key] = v
			} else {
				return fmt.Errorf("missing annotation %s", elt.Annotation)
			}
		}
	} else if len(bmhData.Spec.FromAnnotations) > 0 {
		return fmt.Errorf("no annotations but some extracted")
	}
	bmhDataContext["name"] = object.GetName()
	bmhDataContext["namespace"] = object.GetNamespace()
	return nil
}

func buildIpamContext(
	cl client.Client,
	log logr.Logger,
	bmhData *bmhdatav1beta1.BmhData,
	object metav1.Object,
	gvk schema.GroupVersionKind,
) (bool, map[string]interface{}, map[string]string, error) {
	bmhDataContext := make(map[string]interface{})
	bmhAnnotationsAddons := make(map[string]string)

	ctx := context.Background()
	spec := bmhData.Spec
	ipPoolNames := map[string]bool{}
	for _, source := range [][]bmhdatav1beta1.FromIPPool{
		spec.IpAddressesFromIPPool,
		spec.PrefixesFromIPPool,
		spec.GatewaysFromIPPool,
		spec.DnsServersFromIPPool,
	} {
		for _, elt := range source {
			ipPoolNames[elt.Name] = true
		}
	}
	if len(ipPoolNames) == 0 {
		return false, bmhDataContext, bmhAnnotationsAddons, nil
	}
	requeue := false
	ipPools := map[string]*m3ipam.IPPool{}
	for ipPoolName := range ipPoolNames {
		var ippool = &m3ipam.IPPool{}
		key := types.NamespacedName{
			Name:      ipPoolName,
			Namespace: object.GetNamespace(),
		}
		err := cl.Get(ctx, key, ippool)
		if err != nil {
			log.Error(err, "cannot Access IPPool", "IPPool", ipPoolName)
			return true, nil, nil, err
		}
		ipPools[ipPoolName] = ippool
	}
	for _, request := range spec.PrefixesFromIPPool {
		ippool := ipPools[request.Name]
		if ippool == nil {
			continue
		}
		bmhDataContext[request.Key] = strconv.Itoa(ippool.Spec.Prefix)
		bmhAnnotationKey := getBmhAnnotationKey(request.Key)
		bmhAnnotationsAddons[bmhAnnotationKey] = strconv.Itoa(ippool.Spec.Prefix)
		log.Info("DEBUG - bmhAnnotationsAddons", "request.Key", request.Key,
			"bmhAnnotationKey", bmhAnnotationKey, "prefix", strconv.Itoa(ippool.Spec.Prefix))
	}
	for _, request := range spec.GatewaysFromIPPool {
		ippool := ipPools[request.Name]
		if ippool != nil && ippool.Spec.Gateway != nil {
			bmhDataContext[request.Key] = string(*ippool.Spec.Gateway)
			bmhAnnotationKey := getBmhAnnotationKey(request.Key)
			bmhAnnotationsAddons[bmhAnnotationKey] = string(*ippool.Spec.Gateway)
			log.Info("DEBUG - bmhAnnotationsAddons", "request.Key", request.Key,
				"bmhAnnotationKey", bmhAnnotationKey, "gateway", string(*ippool.Spec.Gateway))
		}
	}
	for _, request := range spec.DnsServersFromIPPool {
		ippool := ipPools[request.Name]
		if ippool != nil && ippool.Spec.DNSServers != nil {
			servers := ippool.Spec.DNSServers
			l := make([]string, len(servers))
			// stupid identity conversion to not use unsafe
			for i, e := range servers {
				l[i] = string(e)
			}
			bmhDataContext[request.Key] = l
		}
	}

	for _, request := range spec.IpAddressesFromIPPool {
		if isM3Request(request) {
			var ipamClaim = &m3ipam.IPClaim{}
			var claimName string
			if EnableBMHNameBasedPreallocation {
				claimName = fmt.Sprintf("%s-%s", object.GetName(), request.Name)
			} else {
				claimName = fmt.Sprintf("%s-%s", object.GetName(), sanitize(request.Key))
			}
			key := types.NamespacedName{
				Name:      claimName,
				Namespace: object.GetNamespace(),
			}
			err := cl.Get(ctx, key, ipamClaim)
			if err != nil {
				if k8serrors.IsNotFound(err) {
					err := createM3IPClaim(cl, ctx, claimName, object, gvk, request.Name)
					if err != nil {
						log.Error(err, "cannot create IPClaim", "IPPool", request.Name)
						return true, nil, nil, err
					}
					log.V(1).Info("Missing IPClaim", "claim", claimName)
					requeue = true
					continue
				} else {
					log.Error(err, "cannot get IPClaim", "IPPool", request.Name)
					return true, nil, nil, err
				}
			}
			addrRef := ipamClaim.Status.Address
			if addrRef == nil {
				log.V(1).Info("Address not set for IPClaim", "claim", claimName)
				requeue = true
				continue
			}
			var address = &m3ipam.IPAddress{}
			key = types.NamespacedName{
				Name:      addrRef.Name,
				Namespace: addrRef.Namespace,
			}
			err = cl.Get(ctx, key, address)
			if err != nil {
				log.Error(err, "cannot access IP Address", "key", request.Key)
				return true, nil, nil, err
			}
			bmhDataContext[request.Key] = string(address.Spec.Address)
			bmhAnnotationKey := getBmhAnnotationKey(request.Key)
			bmhAnnotationsAddons[bmhAnnotationKey] = string(address.Spec.Address)
			log.Info("DEBUG - bmhAnnotationsAddons", "request.Key", request.Key,
				"bmhAnnotationKey", bmhAnnotationKey, "ip", string(address.Spec.Address))
		} else {
			err := fmt.Errorf("unssupported claim type %s.%s", request.ApiGroup, request.Kind)
			log.Error(err, "unsupported claim")
			return true, nil, bmhAnnotationsAddons, err
		}
	}
	return requeue, bmhDataContext, bmhAnnotationsAddons, nil
}

func isM3Request(request bmhdatav1beta1.FromIPPool) bool {
	return request.ApiGroup == m3ipam.GroupVersion.Group && request.Kind == "IPPool"
}

func createM3IPClaim(
	cl client.Client,
	ctx context.Context,
	claimName string,
	object metav1.Object,
	gvk schema.GroupVersionKind,
	ippoolName string,
) error {
	namespace := object.GetNamespace()
	var ipClaim = &m3ipam.IPClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      claimName,
			Namespace: namespace,
			OwnerReferences: []metav1.OwnerReference{
				*metav1.NewControllerRef(object, gvk),
			},
		},
		Spec: m3ipam.IPClaimSpec{
			Pool: corev1.ObjectReference{
				Name:      ippoolName,
				Namespace: namespace,
			},
		},
	}
	return cl.Create(ctx, ipClaim)
}

// Metal3IPClaimToBareMetalHost will return a reconcile request for a BareMetalHost if the event is for a
// Metal3IPClaim and that Metal3IPClaim references a BareMetalHost.
func Metal3IPClaimToBareMetalHost(ctx context.Context, obj client.Object) []ctrl.Request {
	requests := []ctrl.Request{}
	if ipclaim, ok := obj.(*m3ipam.IPClaim); ok {
		for _, ownerRef := range ipclaim.OwnerReferences {
			if ownerRef.Kind != "BareMetalHost" {
				continue
			}
			aGV, err := schema.ParseGroupVersion(ownerRef.APIVersion)
			if err != nil {
				continue
			}
			if aGV.Group != bmhv1alpha1.GroupVersion.Group {
				continue
			}
			requests = append(requests, ctrl.Request{
				NamespacedName: types.NamespacedName{
					Name:      ownerRef.Name,
					Namespace: ipclaim.Namespace,
				},
			})
		}
	}
	return requests
}

func ownedBy(bmh *bmhv1alpha1.BareMetalHost) metav1.OwnerReference {
	return metav1.OwnerReference{
		APIVersion: bmh.APIVersion,
		Kind:       bmh.Kind,
		Name:       bmh.Name,
		UID:        bmh.UID,
	}
}

var notValidChar = regexp.MustCompile("[^a-zA-Z0-9-]")

func sanitize(input string) string {
	return strings.ToLower(notValidChar.ReplaceAllString(input, "-"))
}

func getBmhAnnotationKey(input string) string {
	key := strings.ToLower(strings.ReplaceAll(input, "-", "_"))
	return fmt.Sprintf("%s/%s", BmhAnnotationPrefix, key)
}

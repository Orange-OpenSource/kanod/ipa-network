/*
Copyright 2025 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	BmhDataFinalizer = "network.kanod.io/bmh-data"
)

// BmhDataSpec defines the desired state of BmhData
type BmhDataSpec struct {
	// PreprovisioningNetworkTemplate is the name of a secret containing a go
	// template definition of the preprovisioning network configuration
	// in Glean format at the key template
	PreprovioningNetwork string `json:"preprovisioningNetwork,omitempty"`
	// NetworkData is the name of an optional secret containing a go template
	// definition of the network data of the host.
	NetworkData string `json:"networkData,omitempty"`
	// UserData is the name of an optional secret containing a go template
	// definition of the user data of the host.
	UserData string `json:"userData,omitempty"`
	// GenerateMetadata indicates which secrets to generate and link
	GenerateMetadata bool `json:"generateMetadata,omitempty"`
	// IpAddressesFromIPPool is a list of addresses generated for each instance
	// of the template.
	IpAddressesFromIPPool []FromIPPool `json:"ipAddressesFromIPPool,omitempty"`
	// PrefixesFromIPPool retrieves the prefix of the ippools
	PrefixesFromIPPool []FromIPPool `json:"prefixesFromIPPool,omitempty"`
	// GatewaysFromIPPool retrieves gateway associated to ippools
	GatewaysFromIPPool []FromIPPool `json:"gatewaysFromIPPool,omitempty"`
	// DnsServersFromIPPool retrieves dns associated to ippools
	DnsServersFromIPPool []FromIPPool `json:"dnsServersFromIPPool,omitempty"`
	// FromLabels associate keys to values of some labels in the BaremetalHost
	FromLabels []FromLabel `json:"fromLabels,omitempty"`
	// FromLabels associate keys to values of some labels in the BaremetalHost
	FromAnnotations []FromAnnotation `json:"fromAnnotations,omitempty"`
	// Strings associate keys to fixed values
	Strings []StringValue `json:"strings,omitempty"`
}

type FromIPPool struct {
	// Key is the key of the element in the templates or in meta-data
	Key string `json:"key"`
	// Name of the IPPool used
	Name string `json:"name"`
	// Group of the IPPool
	ApiGroup string `json:"apiGroup"`
	// Kind of the IPPool
	Kind string `json:"kind"`
}

type FromLabel struct {
	// Key is the name of the element in the templates or in meta-data
	Key string `json:"key"`
	// Label is the name of the label on the BareMetalHost
	Label string `json:"label"`
}

type FromAnnotation struct {
	// Key is the name of the element in the templates or in meta-data
	Key string `json:"key"`
	// Annotation is the name of the label on the BareMetalHost
	Annotation string `json:"annotation"`
}

type StringValue struct {
	// Key is the name of the element in the templates or in meta-data
	Key string `json:"key"`
	// Value is the fixed value associated
	Value string `json:"value"`
}

// BmhDataStatus defines the observed state of BmhData
type BmhDataStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:path=bmhdatas,scope=Namespaced
// BmhData is the Schema for the bmhdatas API
type BmhData struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   BmhDataSpec   `json:"spec,omitempty"`
	Status BmhDataStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// BmhDataList contains a list of BmhData
type BmhDataList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []BmhData `json:"items"`
}

func init() {
	SchemeBuilder.Register(&BmhData{}, &BmhDataList{})
}

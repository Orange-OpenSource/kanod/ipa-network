# bmh-data
A resource and controller to set network configuration of the Ironic Python
Agent when used without DHCP.

## Description
The BmhData resource holds a pointer to templates and some resources coming
either from IPPools or from fields in the baremetalhost. The
templates are go templates parameterized by the resource keys.

The expansion is done for each BareMetalHost in the same namespace having a
label `kanod.io/bmh-data` containing the name of the resource.
The result of the expansion is put in a secret refered by the
`preprovisioningNetworkDataName` field of the BareMetalHost for preprovioning
datas or `networkData.name` field for network data.

A meta-data is also automatically generated.

The BareMetalHost should be paused during the generation of the network data.
The annotation `kanod.io/bmh-data-version` on the BareMetalHost contains
the resource version of the IpaNetwork. It can also be used as a barrier for
removing the pause annotation.

## License

Copyright 2023 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

